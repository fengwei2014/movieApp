import Vue from 'vue'
import Vuex from 'vuex'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {}, // store 实例的根状态对象，用于定义共享的量，类似Vue 实例中的data
  actions: {}, // 动作，向store 发出调用通知，执行本地或者远程一个操作，methods
  getters: {}, // 读取器，外部程序通过他获取变量的具体值，相当于 computed
  mutations: {}, // 修改器，用于修改state 中定义的状态变量
  modules: {}, // 注入子模块
  strict: debug, // 调试模式还是生产模式
  plugin: [] // 加入运行期插件
})

// mapGetters 动态方法生成器
// 生成将 store.getter 方法映射为Vue实例的computed
