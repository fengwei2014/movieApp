// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import App from './App'
import store from './store'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import 'normalize.css'

Vue.config.productionTip = false
// base components regirest before new
const requireComponent = require.context(
  // look for files in the current directory
  '.',
  false, // do not look in subdirectories
  /_base-[\w-]+\.vue$/ // file reg
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)
  const componentName = upperFirst(
    camelCase(
      fileName.replace(/^\.\/_/, '').replace(/.\w+$/, '')
    )
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
